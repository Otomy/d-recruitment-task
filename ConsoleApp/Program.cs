﻿using CronExpressionParser.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CronExpressionParser.ConsoleApp
{
  class Program
  {
    static void Main(string[] args)
    {
      if (args.Length > 0 && (args[0] == "?" || args[0] == "-?" || args[0] == "--help"))
      {
        PrintHelp();
        return;
      }

      var validationResponse = Validator.Validate(args);

      if(!validationResponse.IsValid)
      {
        PrintInvalidInput();
        return;
      }

      var parseResponse = Parser.Parse(validationResponse.ClassifiedInput);

      PrintParseRespone(parseResponse);

    }

    private static void PrintHelp()
    {
      Console.WriteLine("Please type space separated 6 arguments after app name. First 5 should be cron numbers, the last one should be program path. Examples:");
      Console.WriteLine("./ParseCron \"*/15 0 1,15 * 1-5 /usr/bin/find\"");
      Console.WriteLine("./ParseCron 1 1 1 1 1 /boom");
    }

    private static void PrintInvalidInput()
    {
      Console.WriteLine("Invalid input - please run \"ParceCron ?\" for instructions.");
    }

    private static void PrintParseRespone(ParserResponse logicResponse)
    {
      WriteOutputLine("minutes", logicResponse.Minutes);
      WriteOutputLine("hour", logicResponse.Hours);
      WriteOutputLine("day of month", logicResponse.DaysOfMonth);
      WriteOutputLine("month", logicResponse.Months);
      WriteOutputLine("day of week", logicResponse.DaysOfWeek);
      WriteOutputLine("command", null, logicResponse.Command);
    }

    private static void WriteOutputLine(string firstColumnString, List<int> secondColumnInts, string? secondColumnString=null)
    {
      var result = new StringBuilder();

      result.Append(firstColumnString);
      for(int i=firstColumnString.Length;i<14;i++)
        result.Append(' ');

      if(secondColumnInts != null)
      foreach(var i in secondColumnInts)
        {
          result.Append(i);
          result.Append(' ');
        }

      if (!String.IsNullOrEmpty(secondColumnString))
        result.Append(secondColumnString);

      Console.WriteLine(result);
    }
  }
}
