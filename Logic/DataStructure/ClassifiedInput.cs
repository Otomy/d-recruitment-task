﻿namespace CronExpressionParser.Logic
{
  public class ClassifiedInput
  {
    public string Minutes { get; internal set; }
    public string Hours { get; internal set; }
    public string DaysOfMonth { get; internal set; }
    public string Months { get; internal set; }
    public string DaysOfWeek { get; internal set; }
    public string Command { get; internal set; }
  }
}