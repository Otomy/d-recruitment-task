﻿using System.Collections.Generic;

namespace CronExpressionParser.Logic
{
  public class ParserResponse
  {
    public List<int> Minutes { get; set; }
    public List<int> Hours { get; set; }
    public List<int> DaysOfMonth { get; set; }
    public List<int> Months { get; set; }
    public List<int> DaysOfWeek { get; set; }
    public string Command { get; set; }
  }
}