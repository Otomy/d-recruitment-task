﻿namespace CronExpressionParser.Logic
{
  public class ValidateResponse
  {
    public ClassifiedInput ClassifiedInput { get; internal set; }
    public bool IsValid { get; set; }
  }
}