﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CronExpressionParser.Logic
{
  public class Parser
  {
    public static ParserResponse Parse(ClassifiedInput input)
    {
      var result = new ParserResponse();

      result.Minutes = ParseUnitString(input.Minutes, 0,59);
      result.Hours = ParseUnitString(input.Hours, 0,24);
      result.DaysOfMonth = ParseUnitString(input.DaysOfMonth, 1,31);
      result.Months = ParseUnitString(input.Months, 1, 12);
      result.DaysOfWeek = ParseUnitString(input.DaysOfWeek, 1,7);

      result.Command = input.Command;

      return result;
    }

    private static List<int> ParseUnitString(string unitString,int minValue, int maxValue)
    {
      var result = new List<int>();

      try
      {
        unitString = unitString.Replace("*", $"{minValue.ToString()}-{maxValue.ToString()}");

        foreach (string commaSeparated in unitString.Split(','))
        {

          if (commaSeparated.Contains('-'))
          {
            string slashSeparated = commaSeparated;
            int? step = null;

            if (commaSeparated.Contains('/'))
            {
              var everySplit = commaSeparated.Split('/');

              slashSeparated = everySplit[0];
              step = Int32.Parse(everySplit[1]);
            }

            var rangeLimits = slashSeparated.Split("-");
            int rangeStart, rangeEnd;
            if (Int32.TryParse(rangeLimits[0], out rangeStart) && Int32.TryParse(rangeLimits[1], out rangeEnd))
            {
              if (!step.HasValue)
              {
                result.AddRange(Enumerable.Range(rangeStart, rangeEnd));
              }
              else
              {
                for (int i = rangeStart; i <= rangeEnd; i += step.Value)
                  result.Add(i);
              }
            }
          }
          else
          {
            int parsedInt;
            if (Int32.TryParse(commaSeparated, out parsedInt))
            {
              result.Add(parsedInt);
            }
          }
        }
      }
      catch
      {
        ;
      }

      return result;
    }
  }
}
