﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CronExpressionParser.Logic
{
  public class Validator
  {
    static public ValidateResponse Validate(string args)
    {
      if (String.IsNullOrEmpty(args))
        return new ValidateResponse() { IsValid = false };

      return Validate(args.Split(' '));
    }

    static public ValidateResponse Validate(string[] args)
    {
      var result = new ValidateResponse();

      try
      {
        result.ClassifiedInput = Classify(args);
        result.IsValid = ValidateInput(result.ClassifiedInput);
      }
      catch
      {
        result.IsValid = false;
      }

      return result;
    }

    private static ClassifiedInput Classify(string[] rawInput)
    {
      var result = new ClassifiedInput();

      var input = Clean(rawInput);

      result.Minutes = input[0];
      result.Hours = input[1];
      result.DaysOfMonth = input[2];
      result.Months = input[3];
      result.DaysOfWeek = input[4];
      result.Command = input[5];

      return result;
    }

    private static List<string> Clean(string[] args)
    {

      var extraChars = new char[] { '\'', '\"' };

      var result = new List<string>();

      foreach (string rawArg in args)
      {
        foreach (var splitted in rawArg.Split(' '))
          result.Add(splitted.Trim(extraChars));
      }

      return result;
    }

    private static bool ValidateInput(ClassifiedInput input)
    {
      try
      {
        var cronExpressions = new List<string>()
        {
          input.Minutes,
          input.Hours,
          input.DaysOfMonth,
          input.DaysOfWeek,
          input.Months
        };

        if (cronExpressions.Any(s => String.IsNullOrEmpty(s)))
          return false;

        if (cronExpressions.Any(s => s.Any(c => !Char.IsDigit(c) && c != '*' && c != '/' && c != ',' && c != '-')))
          return false;

        return true;
      }
      catch
      {
        return false;
      }
    }
  }
}
