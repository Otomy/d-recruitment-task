
## What is this?

This is a simple app parsing given cron input. You should pass a cron input as a parameter and it will print out detailed information about the parsed setup.

  

## How to run?

To run this application please go to the `ConsoleApp\bin\Release\netcoreapp3.1\{your OS}` directory within this project.

Then you can simply run the  `ParseCron` command with parameters.

For example:

`./ParseCron "*/15 0 1,15 * 1-5 /usr/bin/find"`

Please note that the binaries haven't been uploaded into the bitbucket due to their side.
  

## How to build the binaries?

The app is written in .NET Core framework, for which Visual Studio is the common IDE.

If you have Visual Studio installed you will be able to build binaries yourself. The simplest way to do it is to open the solution in VS and then run `dotnet  publish -c Release -r osx-x64 -p:PublishReadyToRun=true` command in `Package Manager Console` window.
  

## What stack has been used?

The app is written in .NET Core framework with C# language. It can be published to all operation systems.

I haven't install any additional library apart from what comes with the framework.

  

## Code structure

The app is divided into three libraries.

The `ConsoleApp` handles the interface of the app. It is runnable. It can read parameters and it prints out the result to the user.

The `Logic` is the library providing actual data processing. It defines the structures of the data and implements the app's logic. It is ready to be used by any interface.

I believe the `LogicUnitTests` is self-explanatory.

  

## Comments

App is written to handle the specific input such as the one shown in the task's instruction.

It doesn't cover the cases of optional parameters. It can't handle the start at x, add y input (x/y).

Currently it doesn't support logging and configuration.