using CronExpressionParser.Logic;
using NUnit.Framework;
using System.Linq;

namespace CronExpressionParser.LogicUnitTests
{
  public class ComplexInputUnitTest
  {
    string inputString = "10-59/8 9 1,3,5 */2 1-5 /test";
    private ParserResponse logicResponse { get; set; }

    [SetUp]
    public new void Setup()
    {
      var validationResponse = Validator.Validate(inputString);
      logicResponse = Parser.Parse(validationResponse.ClassifiedInput);
    }

    [Test]
    public void Minutes()
    {
      Assert.AreEqual(logicResponse.Minutes.Count, 7);

    }

    [Test]
    public void Hours()
    {
      Assert.AreEqual(logicResponse.Hours.Count, 1);

      Assert.IsTrue(logicResponse.Hours.Contains(9));
    }

    [Test]
    public void DayOfMonth()
    {
      Assert.AreEqual(logicResponse.DaysOfMonth.Count, 3);

      Assert.IsTrue(logicResponse.DaysOfMonth.Contains(1));
      Assert.IsTrue(logicResponse.DaysOfMonth.Contains(3));
      Assert.IsTrue(logicResponse.DaysOfMonth.Contains(5));
    }

    [Test]
    public void Months()
    {
      Assert.AreEqual(logicResponse.Months.Count, 6);

      Assert.IsTrue(logicResponse.Months.Contains(3));

    }

    [Test]
    public void DaysOfWeek()
    {
      Assert.AreEqual(logicResponse.DaysOfWeek.Count, 5);


      Enumerable.Range(1, 5).ToList().ForEach(i =>
        Assert.IsTrue(logicResponse.DaysOfWeek.Contains(i))
      );
    }

    [Test]
    public void Command()
    {
      Assert.AreEqual(logicResponse.Command, "/test");
    }
  }
}