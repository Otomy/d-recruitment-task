using CronExpressionParser.Logic;
using NUnit.Framework;
using System.Linq;

namespace CronExpressionParser.LogicUnitTests
{
  public class InstructionExampleWithParamsInputUnitTests
  {
    string[] inputParams = new string[] { "*/15", "0", "1,15", "*", "1-5", "/usr/bin/find" };
    private ParserResponse logicResponse { get; set; }

    [SetUp]
    public new void Setup()
    {
      var validationResponse = Validator.Validate(inputParams);
      logicResponse = Parser.Parse(validationResponse.ClassifiedInput);
    }

    [Test]
    public void Minutes()
    {
      Assert.AreEqual(logicResponse.Minutes.Count, 4);

      Assert.IsTrue(logicResponse.Minutes.Contains(0));
      Assert.IsTrue(logicResponse.Minutes.Contains(15));
      Assert.IsTrue(logicResponse.Minutes.Contains(30));
      Assert.IsTrue(logicResponse.Minutes.Contains(45));
    }

    [Test]
    public void Hours()
    {
      Assert.AreEqual(logicResponse.Hours.Count, 1);

      Assert.IsTrue(logicResponse.Hours.Contains(0));
    }

    [Test]
    public void DayOfMonth()
    {
      Assert.AreEqual(logicResponse.DaysOfMonth.Count, 2);

      Assert.IsTrue(logicResponse.DaysOfMonth.Contains(1));
      Assert.IsTrue(logicResponse.DaysOfMonth.Contains(15));
    }

    [Test]
    public void Months()
    {
      Assert.AreEqual(logicResponse.Months.Count, 12);

      Enumerable.Range(1, 12).ToList().ForEach(i =>
        Assert.IsTrue(logicResponse.Months.Contains(i))
      );
    }

    [Test]
    public void DaysOfWeek()
    {
      Assert.AreEqual(logicResponse.DaysOfWeek.Count, 5);


      Enumerable.Range(1, 5).ToList().ForEach(i =>
        Assert.IsTrue(logicResponse.DaysOfWeek.Contains(i))
      );
    }

    [Test]
    public void Command()
    {
      Assert.AreEqual(logicResponse.Command, "/usr/bin/find");
    }
  }
}